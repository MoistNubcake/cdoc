---
title: CDOC(1) cdoc-moist 0.0.1 | cdoc manual
date: 4-12-2021
---

# NAME
cdoc - search and update cdoc database 


# SYNOPSIS
cdoc [OPTION]... search KEYWORD...

cdoc [OPTION]... update FILE...

cdoc [OPTION]... create DIR


# DESCRIPTION
cdoc is a tool to quickly create and search through documentation. The
documentation should be writting in (pandoc extended) markdown.

## cdoc search KEYWORD...
Search the database for keywords

## cdoc update FILE...
Update the database with file FILE

## cdoc create DIR
Create a new index file for the database under DIR


# OPTIONS

`--index FILE`    
:   Use FILE as the index file. If the index file is a relative path the
    resulting path is `<database dir>` + `<FILE>`.

`--dir DIR`
:   Set the database directory to DIR. If this option is omitted the enviroment is
    checked for `CDOC_DB`. The fallback directory is `~/cdoc`

`-h, --help`
:   Show help message and exit

`--version`
:   Show program's version number and exit


# AUTHOR
Tom Stegeman, Wouter Stegeman


# BUGS AND NOTES
Report any bugs to <stegeman_tom@hotmail.com> or <wouter.stegeman@gmail.com>


# COPYRIGHT
Copyright (C) '2021' 'Tom Stegeman, Wouter Stegeman'

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


# SEE ALSO
 -
