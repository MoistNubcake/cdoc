# manpages

Do not write manpages by hand. Instead write the `*.md` files and convert them
to `*.1` by using pandoc, e.g.:

`pandoc -s -f markdown -t man cdoc.md > cdoc.1`
