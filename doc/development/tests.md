# Executing test suites
Run tests as:

`python -m unittest discover -s tests/unit` and
`python -m unittest discover -s tests/integration`

If the tests are not run from the project top level directory add the `-t`
option to specify the top level directory and adjust the starting point (`-s`)
accordingly. For example running tests from `src/`:

`python -m unittest discover -s ../tests/unit -t ..`

# Executing a single test
To execute a single test execute from the package top level directory use for
example:

`python  -m unittest tests.integration.test_cdoc.CdocTestCase.test_dump_index`

This will only execute the `test_dump_index` test and exit.
