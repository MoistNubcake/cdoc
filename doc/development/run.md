# Manual run

To run the cdoc module without installing use:

`cd src/cdoc; python cdoc.py <SUBCOMMAND>`

To see what subcommand are available use:

`cd src/cdoc; python cdoc.py --help`
