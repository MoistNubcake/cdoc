# Installing with pip (recommended)

If a python wheel is not created run:

`python setup.py bdist_wheel`

This will create a `*.whl` file in `dist/`

To install the wheel run:

`pip install dist/cdoc_moist-*.whl` 

where `*` is the package version, python version and pure/non-pure distribution
suffix.

# Installing with setuptools
To install the package wihout pip run
`python setup.py install`

# Requirements
python >= 3.9
