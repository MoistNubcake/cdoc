from setuptools import setup
from setuptools import find_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name='cdoc-moist',
    version="0.0.1",
    author="Tom Stegeman, Wouter Stegeman",
    author_email="stegeman_tom@hotmail.com, wouter.stegeman@gmail.com",
    description="",
    long_description=long_description,
    long_description_content_type="markdown",
    url="",
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    package_data={'': ['ro-data/*', 'wr-data/*']},
    data_files=[('share/bash-completion/completions/',
                ['completions/cdoc'])],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Lesser General Public License v3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.9',

    entry_points={
        'console_scripts': [
            'cdoc = cdoc.cdoc:main',
        ],
    }

)
