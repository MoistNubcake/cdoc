"""
cdoc is a program to quickly search through a cdoc database based, a directory
containing c-documents (files with an '.cdoc' extention).

A c-document is a (pandoc extended) markdown file with keyword metadata
contained a yaml header. Other optional metadata is contained in a markdown div
block:

    ::: {<attribute>=<value> ...}

Attributes hold extra information about a c-document. Keyword searches can be
confined by selecting only files with certain attribute=value combinations (see
cdoc search --help).

Keyword searches are done by a python dictionary look-up. To enable keyword
searches first a index dictionary has to be created by calling:

    cdoc create [<cdoc-database>]

This subcommand will index the cdoc database by creating the keyword dictionary
and dumps the dictionary automatically to (by default)
<cdoc-database>/index.dump in json format. If the cdoc-database path is omitted
the CDOC_DB enviroment variable is used. If not ~/cdoc is assumed as the
database path.

To update the index to include a new file after it was already created use the
'update' subcommand and the file to be added to the index:

    cdoc update <c-cdocument>

An already added document with update meta data should also be readded by the
'update' subcommand to pickup on the new metadata. The updated keyword
dictionary will be automatically be dumped to the index.dump file.

To update a whole database directory run the subcommand on a cdoc database or
one of its subdirectories:

    cdoc update [<cdoc-database>]

c-documents with a newer modification date compared to the index.dump file be
readded to the keyword dictionary. Note that this might be slower than manually
adding individual files, because files with only non-metadata changes will also
be rescanned.

When the keyword dictionary is created the index can be searched by calling the
'search' subcommand:

    cdoc search <keyword>...

A search will result in a prompt of files names that contain the matching
keywords, ordered by amount of keyword matches. After a selection is made the
path is outputted on stdout which can be piped to a pager to open the document
in.

For all subcommands see also the '--help' switch for optional arguments to
change the default behavior.
"""


import argparse
from os import environ
from pathlib import Path
from json import dump

_DEFAULT_INDEX_NAME = "index.dump"
__version__ = 0.1
__author__ = "Tom Stegeman, Wouter Stegeman"


def search_database(args: object) -> dict:
    """
    Search database based on keywords. Returns a dictionary of filenames that
    contain the keywords as key and the amount of hits as value.

    args.keywords:  list of keyword strings
    args.dir     :  str or Path object of database directory

    return { "<file_path>" : hits }
    """

    raise NotImplementedError("keywords: " + str(args.keywords))

    database_path = Path(args.dir)
    database_index = Path(args.index)
    read_index(database_index)
    file_hits = {}

    return file_hits


def read_index(index_file: Path):
    """
    Read an index dump file and return a keyword dictionary.

    index_file: Index file to read in

    return  {
                '<keyword>' : [ ('<path>',
                              {'<metadata-attribute>' : 'value' }),
                              ],
            }
    """
    raise NotImplementedError()


def create_index(args: object) -> None:
    """
    Create a keyword dictionary and dump the dictionary to an index dump file

    args:               object containing 'dir' and 'index' attribute
        args.dir:       string containing base directory of the database
        args.index:     index file path to dump to
    """
    dict_index = create_index_dict(args)
    dump_index(dict_index, args.index)


def dump_index(dictionary: dict, index_file: Path) -> None:
    """
    Dump the current keyword dictionary to index_file in json format

    dictionary: keyword dictionary with the structure:
                    '<keyword>' : [ ('<path>',
                                    {'<metadata-attribute>' : 'value' }),
                                  ],
    index_file: index file to dump to
    """
    with Path(index_file).open(mode='w') as fp:
        dump(dictionary, fp)


def extract_keywords_from_cdoc_file(yaml_block: str) -> set:
    keywords_string = ""
    is_extracting_keywords = False

    for line in yaml_block:
        if is_extracting_keywords is False:
            pos_keywords = line.find("keywords")
            pos_colon = line.find(":", pos_keywords + 8)
            pos_bracket = line.find("[", pos_colon + 1)

            if pos_keywords == -1 or pos_colon == -1 or pos_bracket == -1:
                continue
            else:
                keywords_string += line[pos_bracket + 1:]
                is_extracting_keywords = True
        else:
            pos_closing_bracket = line.rfind("]")

            if pos_closing_bracket == -1:
                keywords_string += line
            else:
                keywords_string += line[:pos_closing_bracket]
                break

    keywords = set()
    keyword = ""

    for char in keywords_string:
        if char == "," or char == " ":
            keywords.add(keyword)
            keyword = ""
        else:
            keyword += char

    keywords.add(keyword)
    keywords.remove("")

    return keywords


def extract_metadata_from_cdoc_file(metadata: int) -> dict:
    print("here")
    metadata = metadata[metadata_line.find("{") + 1: -1]
    is_reading_attribute = True
    attribute = ""
    value = ""
    attribute_to_value_dictionary = {}

    for char in metadata:
        if char == ' ' or char == '=':
            continue
        elif char == '"':
            is_reading_attribute = not is_reading_attribute

            if is_reading_attribute:
                attribute_to_value_dictionary[attribute] = value
                attribute = ""
                value = ""
        else:
            if is_reading_attribute:
                attribute += char
            else:
                value += char

    return attribute_to_value_dictionary


def create_index_dict(args: object) -> dict:
    """
    Read a cdoc database and return a dictionary that maps the keywords in a
    cdoc to the cdoc itself and its metadata.

    Keywords are listed in a cdoc yaml header as a list called 'keywords'.
    Metadata is listed in a markdown div block (::: {...}) with the data-*
    attribute syntax.

    Required arguments:
        args:           object containing 'dir' attribute
        args.dir:       string containing base directory of the database

    returns {
             '<keyword>' : [ ('<path>',
                             {'<metadata-attribute>' : 'value'}),
                           ],
            }
    """

    index = {}
    extracted_keywords = set()
    extracted_metadata = {}
    yaml_block = ""
    is_reading_yaml_block = False

    database_dir = Path(args.dir)

    for cdoc in database_dir.glob('**/*.cdoc'):
        with open(cdoc) as cdoc_fd:
            for line in cdoc_fd:
                stripped_line = line.strip()

                if stripped_line == "---":
                    is_reading_yaml_block = not is_reading_yaml_block
                elif is_reading_yaml_block:
                    yaml_block += line
                elif stripped_line[0: 3] == ":::" and stripped_line[-1] == "}":
                    # This line contains the metadata. After the metadata is
                    # extracted we have both metadata and keywords and we move
                    # on to the next file.
                    #extracted_metadata = extract_metadata_from_cdoc_file(line)
                    break

            extracted_keywords.union(extract_keywords_from_cdoc_file(yaml_block))

            for keyword in extracted_keywords:
                if keyword not in index:
                    index[keyword] = []

                index[keyword].append((cdoc, extracted_metadata))

            yaml_block = ""
            extracted_keywords.clear()
            extracted_metadata.clear()

    return index


def update_index(args):
    """
    Update the dump index file args.index with args.files and return the new
    keyword dictionary.

    args.files: list of cdoc files
    args.index: index file to update
    """
    index = read_index(args)

    raise NotImplementedError()

    dump_index(index, args.index)
    return index


def _get_default_db_dir():
    """
    Return cdoc datapath set in enviroment. If not set return ~/cdoc
    """
    try:
        return environ['CDOC_DB']
    except KeyError:
        return Path().home() / "cdoc"


def main():
    """
    Parse command line arguments and call the function associated with the
    subcommand.

    Subcommands:
    search <keyword> ...
        Call search_database. Print an ordered list of files containing
        the keywords and request user input to select a file. Return the path
        of the file to stdout.

        args.keywords: list of keywords

    update <cdoc_file>...
        Call update_index. Adds the files to the index file.

    create [<cdoc_database_dir>]
        Calls create_index. Read a cdoc database and constructs a keyword: file
        dictonary. This dictionary is dumped to an a file that can later be
        read in again.
    """

    parser = argparse.ArgumentParser(
        description='Search cdoc formatted documentation')
    parser.add_argument('--version', action='version', version='%(prog)s '
                        + str(__version__))
    subcommand_parser = parser.add_subparsers()

    # Database search parser
    parser_search = subcommand_parser.add_parser('search',
                                                 help='search through a cdoc \
                                                 database by keywords')
    parser_search.add_argument('keywords', metavar='KEYWORD', type=str,
                               nargs='+', help='keywords to search for in \
                               database')
    parser_search.add_argument('--dir', metavar='DB_DIR', type=str, nargs=1,
                               help='use DB_DIR as the database directory',
                               default=_get_default_db_dir())
    parser_search.set_defaults(func=search_database)

    # Index create parser
    parser_create = subcommand_parser.add_parser('create',
                                                 help='create an index for a \
                                                 cdoc database')
    parser_create.add_argument('dir', metavar='DIR', type=str, nargs='?',
                               help='update database index with base \
                               directory DIR', default=_get_default_db_dir())
    parser_create.add_argument('--index', metavar='INDEX-FILE', type=str,
                               nargs=1, help='the index file to use',
                               default=_DEFAULT_INDEX_NAME)
    parser_create.set_defaults(func=create_index)

    # Update index parser
    parser_update = subcommand_parser.add_parser('update',
                                                 help='update the database')
    parser_update.add_argument('--index', metavar='INDEX-FILE', type=str,
                               nargs=1, help='relative or absolute path to the \
                               index file', default=_DEFAULT_INDEX_NAME)
    parser_update.add_argument('--dir', metavar='DB_DIR', type=str, nargs=1,
                               help='use DB_DIR as the database directory',
                               default=_get_default_db_dir())
    parser_update.add_argument('files', metavar='FILE', type=str, nargs='+',
                               help='file(s) to update the index with')
    parser_update.set_defaults(func=update_index)

    # Call subcommand function
    args = parser.parse_args()
    try:
        args.func(args)
    except AttributeError as e:
        parser.print_help()
        print("\n(" + str(e) + ")")


if __name__ == "__main__":
    main()
