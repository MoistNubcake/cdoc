import unittest
from src.cdoc import cdoc
from json import dump
from pathlib import Path


class CdocTestCase(unittest.TestCase):

    def setUp(self):
        self.cleanup_files = []
        self.fixtures = Path(Path(__file__).resolve().parents[0]) / "fixtures/"

        self.mock_dict_index = {
                "string": [("dir1/file.cdoc",
                           {"lang": "python", "phylum": "computer-science"})
                           ],
                "module": [("dir2/python-module.cdoc",
                           {"lang": "python", "phylum": "computer-science"}),
                           ("dir1/c-module.cdoc",
                           {"lang": "c", "phylum": "computer-science"})]
                }

        self.addCleanup(self.cleanupIndex)

    def _create_mock_index(self):
        mock_index_file = self.fixtures / "index.dump"
        with open(mock_index_file, "w") as fp:
            dump(self.mock_dict_index, fp)

    def cleanupIndex(self):
        for cleanup_file in self.cleanup_files:
            try:
                cleanup_file.unlink()
            except FileNotFoundError:
                pass

    def test_search_cdoc(self):
        raise NotImplementedError()

    def test_readindex_cdoc(self):
        index = cdoc.read_index()
        self.assertIsInstance(index, dict)
        self.assertDictContainsSubset(index, self.dict_index)

    def test_dump_index(self):
        dump_index = Path(self.fixtures) / "index_new.dump"
        self.cleanup_files.append(dump_index)

        cdoc.dump_index(self.mock_dict_index, dump_index)
        self.assertIsInstance(dump_index, Path)
        self.assertTrue(dump_index.is_file())

    # Test if index gets created
    def test_create_index(self):
        mock_db = Path(self.fixtures) / "example-database"
        mock_db_index = Path(self.mock_db) / "index.dump"

        args = type('', (), {})
        args.dir = mock_db
        args.index = cdoc._DEFAULT_INDEX_NAME
        self.cleanup_files.append(mock_db_index)

        cdoc.create_index(args)
        self.assertTrue(self.new_index.is_file())


if __name__ == '__main__':
    unittest.main()
