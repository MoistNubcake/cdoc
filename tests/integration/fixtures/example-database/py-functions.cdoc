---
title: 'Python Functions'
keywords: [dunder, magic, typing, type hinting,
   default arguments, default parameters]
description: 'Summary of python functions'
---

::: {data-cdoc-class="programming" id="functions" data-phylum="cs" data-os="" data-lang="python"}

# Contents

 * [Basic](#PYFUNCTIONBasic)
 * [Parameters](#PYFUNCTIONParameters)
 * [Type hinting](#PYFUNCTIONTypehinting)
 *  [Function type](#PYFUNCTIONFunctiontype)
 * [Dunder methods](#PYFUNCTIONDunder)
 * [Decorators](#PYFUNCTIONDecorators)
 *  [Decorator arguments ](#PYFUNCTIONDecoratorArgs)
 *  [class and staticmethods](#PYFUNCTIONclassStaticMethods)


# Basic {#PYFUNCTIONBasic}

Defining a function

``` {#simplefunction .python .numberLines caption="A simple function definition"}
def functionName(parameter):
    # function body

```

When a function belongs to a class the first parameter refers to the object
instance itself. This parameter can be any name (resolved during object
creation) but is called `self` by convention. When calling a function from an
class object instance is transformed internally by the class object, that sets
the `self` parameter to the object instance.

# Parameters {#PYFUNCTIONParameters}
Python allows for 4 types of parameters

1. positional parameters

   - example: `def foo(var):`
   - mandatory arguments
   - obligatory defined before namend and arbitrary amount parameters

2. default parameters

   - example: `def foo(var=1):`
   - defined after positional arguments
   - can be overwritten with keyword syntax e.g. `foo(var=2)`
   - when set after arbitrary amount parameters can only be set using the
     keyword function calling syntax. Otherwise can be overwritten as a
     positional parameter.
   - Mutable default parameters act as [static variables][1]. Use for example
   `var=NONE` / `var : dict=NONE` to let the parameter not behave statically.

3. arbitrary amount parameter

   - example:

        ``` {#arbitaryParamsEx .python .numberLines}
        def foo(*args):
            print(args[2])
        ```
     where `*args` is a tuple
   - when set before default parameters the default parameters can only be
   overwritten with the keyword function calling syntax.

4. keyword arbitrary amount parameter

   - example:

       ``` {#arbitraryKeywordsEx .python .numberLines}
       def foo(**kwargs):
           print(kwargs["keyword"])

       foo(keyword="hello world")
       ```
      where `**kwargs` is a dictionary


# Type hinting {#PYFUNCTIONTypehinting}
Python uses [dynamic typing][2]. For API-, documentation and IDE purposes type
hinting conveys the indended parameter types.


``` {#typehint .python .numberLines}
def foo(param1: int, param2: str) -> str:
    #...
```

Defines a function that takes an integer and string and returns a string.

For default arguments the syntax is as follows:

``` {#typehintdefault .python .numberLines}
def foo(param1: int=1, param2: dict=None) -> str:
    #...
```

For the use of `None` see [parameters](#PYFUNCTIONParameters)

Static type checking is possible by using the builtin `type()` function. After a
type mismatch one can raise an `TypeError` exception.

## Function type {#PYFUNCTIONFunctiontype}

``` {#example3 .python .numberLines}
from typing import Callable

def feeder(get_next_item: Callable[[], str]) -> None:
    # Body
```

# Dunder methods {#PYFUNCTIONDunder}

Dunder methods (double underscore methods) are called automatically as a result
of action performed on a object. [See also](#PYCLASSDunder)

Builtin functions often call a dunder method, such as `len()` (`__len__()`),
and `print()` (`__str()__`). Operators such as `<`, `+`, `==` use respectively
the dunder methods `__lt__()`, `__add__()` and `__eq__()`.

# Decorators {#PYFUNCTIONDecorators}
Function definitions can be altered by pass the original function into a new
transforming function that returns a new function. The newly returned function
_can_ call the original function to create a callback interface for the original
function:

``` {#PYFUNCTIONnoDecoratorEx .python .numberLines}
def decoratorFunction(originalFunctionIn):
    print("Inside decoratorFunction")

    def transformingFunction():
        print("Inside transformingFunction")
        originalFunctionIn()

    return transformingFunction


def originalFunction():
    print("Inside originalFunction")

# output: 'Inside decoratorFunction' {#PYFUNCTIONoutput}
originalFunction = decoratorFunction(originalFunction)

# output: 'Inside transformingFunction' {#PYFUNCTIONoutput}
# output: 'Inside originalFunction' {#PYFUNCTIONoutput}
originalFunction()
```

Function decorators are syntax sugar the above mentioned mechanism. A function
decator starts with an `@` sign followed by the name of the transforming
function.


``` {#PYFUNCTIONsimpleDocatorEx .python .numberlines}
def decoratorFunction(originalFunctionIn):
    print("Inside decoratorFunction")

    def transformingFunction():
        print("Inside transformingFunction")
        originalFunctionIn()

    return transformingFunction


# The decorator below is equivalent to: {#PYFUNCTIONThe}
# >>> originalFunction = decoratorFunction(originalFunction)
# output: 'Inside decoratorFunction' {#PYFUNCTIONoutput}
@decoratorFunction
def originalFunction():
    print("Inside originalFunction")


# orignalFunction is bound to transforming function {#PYFUNCTIONorignalFunction}
# output: 'Inside transformingFunction' {#PYFUNCTIONoutput}
# output: 'Inside originalFunction' {#PYFUNCTIONoutput}
originalFunction()
```

## Decorator arguments  {#PYFUNCTIONDecoratorArgs}
If the decorator is called with arguments the inner function becomes the
decorator:

``` {#PYFUNCTIONargDecoratorEx .python .numberlines}
def decoratorFunction(param1):
    print("Inside decoratorFunction")

    def decoratorFunctionInner(originalFunctionIn):

        def transformingFunction():
            print("Inside transformingFunction")
            originalFunctionIn()

        return transformingFunction
    return decoratorFunctionInner


# The decorator below is equivalent to: {#PYFUNCTIONThe}
# >>> originalFunction = decoratorFunction("argumentString")(originalFunction)
# output: 'Inside decoratorFunction' {#PYFUNCTIONoutput}
@decoratorFunction("argumentString")
def originalFunction():
    print("Inside originalFunction")


# orignalFunction is bound to transformingFunction {#PYFUNCTIONorignalFunction}
# output: 'Inside transformingFunction' {#PYFUNCTIONoutput}
# output: 'Inside original function' {#PYFUNCTIONoutput}
originalFunction()
```

## class and staticmethods {#PYFUNCTIONclassStaticMethods}

[1]: https://docs.python-guide.org/writing/gotchas/#mutable-default-arguments
[2]: https://docs.python.org/3/library/typing.html
[3]: https://docs.python.org/3/library/typing.html#callable

:::
