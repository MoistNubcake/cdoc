---
title: 'Pythonic program design'
keywords: [pythonic, design, style, EAFP, interface, ducktyping]
description: 'Pythonic program design summary'
---

::: {data-cdoc-class="programming" id="pythonic" data-phylum="computer-science" data-os="" data-lang="python"}

# Contents

 * [Ducktyping](#PYTHONICDucktyping)
 * [EAFP](#PYTHONICEAFP)
 * [Interface consequences](#PYTHONICconsequences)
 * [Empower the user; A special case is not special enough](#PYTHONICempower)

# Ducktyping {#PYTHONICDucktyping}

Ducktyping is a design pattern. Objects not recognized by their membership
to a class/type but the way they behave: what functions properties and functions
does an object define.

In general do not care what type or classes objects belong to but raise and
handle exceptions ([see EAFP](#PYTHONICEAFP))

# EAFP {#PYTHONICEAFP}

It is easier to ask forgiveness than permission

 - Forgiveness: raise exceptions instead of checking conditions
 - Permission: check conditions, look before you leap

## Interface consequences {#PYTHONICconsequences}
1. Using an interface

   Do not check if a method exists (hasattr(obj, 'attr') and
   callable(obj.attr)). Instead call the function and let the code user handle
   a possible NameError (not defined) or TypeError (argument count does not match).

2. Implementing an interface

   Do not check for isinstance() if it is not integral to a method but follow
   the Duck Typing principle.

## Duck typing exceptions
Notable exceptions for duck typing are:

 - NameError
  - UnboundLocalError

Variable is not defined

 - TypeError

Name is not of required type; e.g. object is not `callable`

 - AttributeError

Object has not attribute while getting


## Math operation exceptions
 - ArithmeticError
  - FloatingPointError
  - OverflowError
  - ZeroDivisionError

## Indexing exceptions 
 - LookupError
  - IndexError (array out of bounds)
  - KeyError (dictionary key not present)


For a full list of builtin exceptions [see](py-exceptions)

# Empower the user; A special case is not special enough {#PYTHONICempower}

Eventhough `__new__()` and `__init__` fill the attribute dictionary for the
object instance, nothing prevents setting attributes by the object instance
itself, e.g. `myobject.a = 1` eventhough `a` was never created at objection
creation.

In other words the attribute dictionary is just a regular dictionary
without any special write permission properties. `__slots__ = [<attributes]` or
the `__setattr__` method can be used as strategies to inhibit this type of
behavior, but in general it is against the python philosophy.

:::
