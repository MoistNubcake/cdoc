---
title: 'Python keywords'
keywords: [with, __iter__, __next__, iter, next, for, try, except, raise,
exception, finally]
description: 'list and explanation of python keywords'
---

::: {data-cdoc-class="programming" id="keywords" data-phylum="computer-science" data-os=""data-lang="python"}

# Contents {#PYKEYWORDScontents}

 * [for](#for-pykeyword)
 * [try](#try-pykeyword)
 * [with](#with-pykeyword)

[See also][1]

# for {#for-pykeyword}
The `for` keyword takes an iterable object to iterate over its values. For
example a list is an iterable object that can be iterated over as shown below:

```{.python #py-for-example}
mylist = [1, 2, 3]

for i in mylist:
    print(i)
```

An iterable object is defined by implementing two dunder methods:

1. `__iter__`
Initializes values if any and returns the iterable object itself

2. `__next__`
Updates values and returns the next object. The object gets bound to the names
supplied in the `for` statement (e.g. `i` in the example above). The for
statement excepts the `StopIteration` exception to stop the for loop.

To create a custom iterable object implement the two dunder methods in a class
object:

```{.python #py-custom-iter-object}
class MyClass(object):

    def __init__(self):
        self.string1 = "hello"
        self.string2 = "world"

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        if self.index == 0:
            result = self.string1
        elif self.index == 1:
            result = self.string2
        else:
            raise StopIteration

        self.index += 1
        return result

myobject = MyClass()
for mystr in myobject:
    print(mystr)
```

# while

```
while <expression>:
    <suite>

```

# try {#try-pykeyword}
The `try-except-else-finally` construct can change the program execution flow.

- when an [exception][2] in a `try` block __is raised__ the matching except block is
executed. If no matching except block is found the raised exception is
propagated down.

- If an exception is __not raised__ a optional `else` block is executed.

- A optional `finally` block is __whether or not an exception is raised__.

An example is shown below:

```{.python}
import sys

class MyException(Exception):
    pass

try:
    if int(sys.argv[1]) == 42:
        raise MyException("42 error!")
except IndexError as err:
    print("No argument passed to the program: ", err)
except Exception as err:
    print("Argument to program 42 passed exception: ", err)
else:
    print("Argument to program was not 42")
finally:
    print("Life goes on")
```

see also [exceptions](py-exceptions)

# with {#with-pykeyword}
Use a context manager for an object object usually to encapsulate `try-except`
statements that should be present by default. The object should implement the
following two dunder methods to define a context manager:

`__enter__()`
:   start of execution. If the `with-as` construct is used the `as <name>` is
    assigned to the return value of this method.

`__exit__()`
:   end of execution. If an exception is raised as well as the end of execution
    of the suite this method is called.

_Usage_
```{.python}
with <with_object> [as <name>]:
    <suite> 
```

Multiple `<with_object> [as <name>]` can be stringed together seperated by
commas.

_Example_
```{.python}
with open('./mytestfile) as file: 
    filecontents = file.read()
```

Here a failed open, read or write calls the exit routine automatically, closing
the file.

:::

[1]: https://docs.python.org/3/reference/compound_stmts.html
[2]: https://docs.python.org/3/tutorial/errors.html
